package com.example.shualeduri1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_content_table.*

class ContentTableActivity : AppCompatActivity() {

    private lateinit var adapter: WeatherViewAdapter
    private var items = ArrayList<CityModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_table)
        init()
    }
    private fun init(){
        weather_List.layoutManager = LinearLayoutManager(this)
        add_city.setOnClickListener {
            getCity(add_city_text.text.toString())
        }
    }

    private fun getCity(str:String){
        val key:String = "1d5dac4caab9c7ae47525492ac408a22"
        val units:String = "metric"
        adapter = WeatherViewAdapter(items, this)
        weather_List.adapter = WeatherViewAdapter(items, this)
        DataLoader.getRequest(str,key,units,object:CustomCallback {
            override fun onSucces(result: String) {
                    val model = Gson().fromJson(result, CityModel::class.java)
                    d("model",model.toString())
                    items.addAll(listOf(model))
                    d("model2",items.toString())
                    adapter.notifyDataSetChanged()
            }
        })
    }

}

private fun <E> ArrayList<E>.addAll(elements: List<CityModel.Main>) {

}


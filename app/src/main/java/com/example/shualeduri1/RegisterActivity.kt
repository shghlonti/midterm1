package com.example.shualeduri1

import android.content.Intent
import android.os.Bundle
import android.util.Patterns
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }
    fun init(){
        auth = Firebase.auth
        registerAccount.setOnClickListener {
            onRegister()
        }
        LogInButtonRegister.setOnClickListener {
            startActivity(Intent(this,MainActivity::class.java))
            finish()
        }
    }

    private fun onRegister(){
        if(emailEditTextRegister.text.toString().isEmpty()){
            emailEditTextRegister.error = "Please Enter Email"
            emailEditTextRegister.requestFocus()
            return
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(emailEditTextRegister.text.toString()).matches()){
            emailEditTextRegister.error = "Please Enter Valid Email"
            emailEditTextRegister.requestFocus()
            return
        }
        if(passwordEditTextRegister.text.toString().isEmpty()){
            passwordEditTextRegister.error = "Please Enter Password"
            passwordEditTextRegister.requestFocus()
            return
        }
        auth.createUserWithEmailAndPassword(emailEditTextRegister.text.toString(),passwordEditTextRegister.text.toString())
            .addOnCompleteListener(this){ task ->
                if (task.isSuccessful){
                    startActivity(Intent(this,MainActivity::class.java))
                    finish()
                }else{
                    Toast.makeText(baseContext,"Failed To Register, Try Again",
                        Toast.LENGTH_SHORT).show()
                }
            }

    }


}
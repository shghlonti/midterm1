package com.example.shualeduri1


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.util.Patterns
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_register.*

class MainActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        auth = Firebase.auth

        SignupButton.setOnClickListener {
            startActivity(Intent(this,RegisterActivity::class.java))
            finish()
        }

        LogInButton.setOnClickListener {
            onLogin()
        }

    }

    public override fun onStart() {
        super.onStart()
//        Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        updateUI(currentUser)
    }

    private fun updateUI(currentUser: FirebaseUser?){
        if(currentUser!=null){
            startActivity(Intent(this,DashboardActivity::class.java))
            d("User", currentUser.toString())
            finish()
        }
        else{
            Toast.makeText(baseContext,"Log In Failed",Toast.LENGTH_SHORT).show()
        }
    }

    private fun onLogin(){
        if(emailEditText.text.toString().isEmpty()){
            emailEditText.error = "Please Enter Email"
            emailEditText.requestFocus()
            return
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(emailEditText.text.toString()).matches()){
            emailEditTextRegister.error = "Please Enter Valid Email"
            emailEditTextRegister.requestFocus()
            return
        }
        if(passwordEditText.text.toString().isEmpty()){
            passwordEditText.error = "Please Enter Password"
            passwordEditText.requestFocus()
            return
        }
        auth.signInWithEmailAndPassword(emailEditText.text.toString(), passwordEditText.text.toString())
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    Toast.makeText(baseContext, "Authentication failed.",
                        Toast.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }
    }


}

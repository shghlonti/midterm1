package com.example.shualeduri1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        auth = Firebase.auth
        init()
    }

    private fun init(){

        logOutButton.setOnClickListener {
            signOut()
        }
        homePageButton.setOnClickListener {
            startActivity(Intent(this,ContentTableActivity::class.java))
        }

        val email:String = auth.currentUser?.email.toString()
        userMail.setText(email)
    }

    private fun signOut(){
        auth.signOut()
        startActivity(Intent(this,MainActivity::class.java))
        d("Activity","started activity")
        Toast.makeText(baseContext, "Signing Out",
            Toast.LENGTH_SHORT).show()
        finish()

    }
}

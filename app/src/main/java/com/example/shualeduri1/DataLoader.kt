package com.example.shualeduri1

import android.util.Log.d

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query


object DataLoader {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://api.openweathermap.org/")
        .build()

    private var service = retrofit.create(ApiRetrofit::class.java)

    fun getRequest(city: String,apikey:String,units:String, customCallback: CustomCallback){
//        val url:String = retrofit.baseUrl().toString()+path
//        d("path",url)
        val call: Call<String> = service.getRequest(city,apikey,units)
        call.enqueue(callBack(customCallback))
    }
    private fun callBack(customCallback: CustomCallback)=  object :Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            d("getRequestFail","${t.message}")
            customCallback.onFailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {
            d("getRequestSuccess","${response.body()}")
            customCallback.onSucces(response.body().toString())
        }

    }
}

interface ApiRetrofit {
    @GET( "data/2.5/weather")
    fun getRequest(@Query("q")city: String, @Query("appid")apiKey: String ,@Query("units")units:String): Call<String>

}


package com.example.shualeduri1

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.weather_recyclerview_layout.view.*

class WeatherViewAdapter(
    private val items: ArrayList<CityModel>,
    contentTableActivity: ContentTableActivity
): RecyclerView.Adapter<WeatherViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewAdapter.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.weather_recyclerview_layout,parent,false))
    }



    override fun getItemCount() = items.size


    override fun onBindViewHolder(holder: WeatherViewAdapter.ViewHolder, position: Int) {
        holder.onBind()
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        private lateinit var model: CityModel
        @SuppressLint("SetTextI18n")
        fun onBind(){
            model = items[adapterPosition]
            itemView.city_name.setText(model.name)
            itemView.city_temp.setText(model.main.temp.toString()+" C")
        }
    }


}